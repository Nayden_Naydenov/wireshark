# Analitzador de protocols Wireshark

- Instal·la i configura Wireshark.
  - sudo add-apt-repository ppa:wireshark-dev/stable
  - sudo apt update
  - sudo apt upgrade -y
  - sudo apt install wireshark
- Carrega la captura "captura1.pcapng" que teniu a la carpeta.

Aconsegueix trobar la següent informació

1. Al protocol ARP:Pots saber quina adreça MAC te l'equip amb adreça 192.168.1.1?
Si, filtres per protocol ARP busques la ip

  ![](assets/README-25e5691c.png)

2. A la sessió ftp:Quin és el password de l'usuari que inicia sessió?Quin nom te el fitxer que es descarrega del servidor?

  ![](assets/README-ee897803.png)

  ![](assets/README-f1bbe8a8.png)

  ![](assets/README-1327d574.png)
3. A la sessió de Telnet:
- Pots veure el que veia l'usuari en connectar al telnet? Explica què és. Quins caràcters composen la nau espacial petita (posar com a resposta)?
  - Si, una nau que es va moven composada per |><>
- Pots connectar al servidor telnet original i explicar que hi ha?
- A quin domini pertany l'adreça on ens connectem?

4. A la sessió ssh:

- Pots saber a quin domini pertany l'adreça del servidor?

  - Si, https://sdf.org/ . L'he averiguat buscant l'ip a internet


- Pots veure el contingut de les dades de la sessió? Enganxa les dades que conté el paquet ssh de longitud total 326 bytes.
  - No, està encriptat

    ![](assets/README-93a586b6.png)

5. Quin és el contingut del fitxer (a partir de la captura extra "captura1.pcapng") que s’ha baixat per FTP?
- A la barra de cerca de wireshark escrivim ftp-data > fem click dret sobre el primer paquet follow > TCP Stream

  ![](assets/README-d090d7c8.png)

- Canviem ASCII per raw > save as > texto.txt

  ![](assets/README-d73baa5a.png)

6. Amb la captura "captura2.pcapng" que s’afegeix, troba el missatge que s’ha enviat amb el protocol de correu sortint.
- Descarregar captura2.pcapng > en la barra superior escrivim smtp.data.fragment

  ![](assets/README-d7647efa.png)

- A la esquina superior esquerra file > export objects > imf... > guardar como > obrir amb edito de txt

  ![](assets/README-9933559d.png)
